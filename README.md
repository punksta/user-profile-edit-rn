# React native edit profile demo app

Simple two screens apps for user profile editing

## prepare project
1. install native ios and android react-native environment https://facebook.github.io/react-native/docs/getting-started
2. `npm ci` to install node_modules
3. `cd ios && pod install  --deployment` to install native ios projects
4. create and start android AVD or connect android device before trying app on android

## npm scripts
### code quality
- `npm run flow` - runs flow types check
- `npm run test` - runs jest tests
- `npm run prettier` - performs code automatic formatting

### run app
1. `npm run start` - stars metro js server
2. `react-native run-ios` - builds and runs ios app
3. `react-native run-android` - builds and runs android app

