/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StatusBar,
    StyleSheet
} from 'react-native';

import {Provider} from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'

import {createAppNavigator} from "./src/navigation";
import {createAppContainer} from "react-navigation";
import configureStore from "./src/configureStore";

const Navigator = createAppNavigator();
const NavigationContainer = createAppContainer(Navigator);

const {
  store, persistor
} = configureStore();

const App = () => {
  return (
      <Provider store={store}>
        <PersistGate persistor={persistor} loading={null}>
          <NavigationContainer style={{flex:1}}/>
        </PersistGate>
      </Provider>
  );
};


export default App;
