// @flow
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

type Props = {
  onPress: () => void,
  title: string,
};
export const UserProfileEditButton = React.memo<Props>(({onPress, title}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          borderTopWidth: 1,
          borderColor: 'rgba(0,0,0,0.67)',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingVertical: 16,
          paddingHorizontal: 16,
        }}>
        <Text
          style={{
            fontSize: 21,
          }}>
          {title}
        </Text>
        <Text>-></Text>
      </View>
    </TouchableOpacity>
  );
});
