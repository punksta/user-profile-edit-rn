// @flow
import React from 'react';
import {View, ScrollView, SafeAreaView} from 'react-native';
import {useSelector} from 'react-redux';
import {UserProfileEditButton} from './UserProfileEditButton';
import {selectCurrentUser} from '../reducers/userProfile';
import {useNavigation} from 'react-navigation-hooks';
import {UserProfileNameHeader} from './UserProfileNameHeader';
import {UserProfileConcatInformation} from './UserProfileContactInformation';

const contactDataOrder = [
  'phoneNumber',
  'email',
  'postalCode',
  'town',
  'streetName',
  'houseNumber',
  'flatNumber',
];

export const UserProfileScreen = () => {
  const user: any = useSelector(selectCurrentUser);
  const {navigate} = useNavigation();

  const goEditProfile = React.useMemo(
    () => () =>
      navigate('EditProfile', {contactInformation: user.contactInformation}),
    [user.contactInformation],
  );

  const userContactData = React.useMemo(
    // $FlowFixMe
    () => contactDataOrder.map(key => [key, user.contactInformation[key]]),
    [user.contactInformation],
  );

  return (
    <SafeAreaView
      style={{
        flex: 1,
      }}>
      <ScrollView>
        <View>
          <UserProfileNameHeader name={user.name} userId={user.id} />
          <UserProfileConcatInformation
            data={userContactData}
            title={'Contact information'}
          />
        </View>
      </ScrollView>
      <UserProfileEditButton
        onPress={goEditProfile}
        title={'Edit my profile'}
      />
    </SafeAreaView>
  );
};
