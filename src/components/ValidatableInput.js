import React, {PureComponent} from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';

type Props = {
  title: string,
  error?: string,
  initialInputValue?: string,
  onValueChanged?: (value: string) => void,
  rootViewStyle?: mixed,
};

type State = {
  inputValue: string,
};
export class ValidatableInput extends PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: props.initialInputValue.toString(),
    };
  }

  onInputChange = inputValue => {
    this.setState({
      inputValue,
    });
    this.props.onValueChanged(inputValue);
  };

  render() {
    return (
      <View style={this.props.rootViewStyle}>
        <Text>{this.props.title}</Text>
        <View
          style={{
            marginTop: 8,
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: 'gray',
            justifyItems: 'center',
            paddingHorizontal: 8,
          }}>
          <TextInput
            value={this.state.inputValue}
            onChangeText={this.onInputChange}
            onEndEditting={this.props.onValueChanged}
            style={{
              minHeight: 40,
            }}
          />
        </View>
        <Text style={{color: 'red', fontSize: 9}}>{this.props.error}</Text>
      </View>
    );
  }

  static defaultProps = {
    initialInputValue: '',
    onValueChanged: () => {},
    rootViewStyle: {
      marginBottom: 8,
      marginTop: 8,
    },
  };
}
