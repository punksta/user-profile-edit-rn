// @flow
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import React, {useState} from 'react';
import {View, ScrollView, SafeAreaView, Button} from 'react-native';
import {ValidatableInput} from './ValidatableInput';
import {getConstants} from './constants';
import type {ContactInformation, User} from '../data/userScheme';
import {selectErrorMessageByPath} from '../data/utils';
import {contactInformationScheme} from '../data/userScheme';
import {useDispatch} from 'react-redux';
import {changeUserProfileContactInformation} from '../actions/userProfile';
import type {ValidationError} from '../data/utils';

export const EditProfileScreen = () => {
  const {goBack} = useNavigation();
  const initialContactInformation: ContactInformation = useNavigationParam(
    'contactInformation',
  );

  const dispatch = useDispatch();

  const [contactInformation, setInfo] = useState(initialContactInformation);

  const [validationError, setError]: [
    ValidationError,
    (ValidationError) => void,
  ] = useState({});

  const saveButtonPress = React.useMemo(
    () => async () => {
      let result;
      try {
        result = await contactInformationScheme.validate(contactInformation, {
          abortEarly: false,
        });
      } catch (validationError) {
        setError(validationError);
      }
      if (!!result) {
        dispatch(changeUserProfileContactInformation(contactInformation));
        goBack();
      }
    },
    [contactInformation],
  );

  return (
    <SafeAreaView
      style={{
        flex: 1,
        marginHorizontal: getConstants().ScreenHorizontalPadding,
      }}>
      <ScrollView>
        <ValidatableInput
          error={selectErrorMessageByPath(validationError, 'phoneNumber')}
          onValueChanged={phoneNumber =>
            setInfo(data => ({...data, phoneNumber}))
          }
          title={'Telephone number'}
          initialInputValue={contactInformation.phoneNumber}
        />
        <ValidatableInput
          title={'Email address'}
          error={selectErrorMessageByPath(validationError, 'email')}
          onValueChanged={email => setInfo(data => ({...data, email}))}
          initialInputValue={contactInformation.email}
        />
        <ValidatableInput
          title={'Post code'}
          error={selectErrorMessageByPath(validationError, 'postalCode')}
          onValueChanged={postalCode =>
            setInfo(data => ({...data, postalCode}))
          }
          initialInputValue={contactInformation.postalCode}
        />

        <View
          style={{
            flexDirection: 'row',
            flex: 1,
          }}>
          <ValidatableInput
            title={'house number'}
            error={selectErrorMessageByPath(validationError, 'houseNumber')}
            rootViewStyle={{flex: 1, marginRight: 8}}
            onValueChanged={houseNumber =>
              setInfo(data => ({...data, houseNumber}))
            }
            initialInputValue={contactInformation.houseNumber}
          />
          <ValidatableInput
            title={'flat number'}
            error={selectErrorMessageByPath(validationError, 'flatNumber')}
            rootViewStyle={{flex: 1, marginLeft: 8}}
            onValueChanged={flatNumber =>
              setInfo(data => ({...data, flatNumber}))
            }
            initialInputValue={contactInformation.flatNumber}
          />
        </View>
        <ValidatableInput
          title={'Street'}
          error={selectErrorMessageByPath(validationError, 'streetName')}
          onValueChanged={streetName =>
            setInfo(data => ({...data, streetName}))
          }
          initialInputValue={contactInformation.streetName}
        />
        <ValidatableInput
          title={'Town'}
          error={selectErrorMessageByPath(validationError, 'town')}
          onValueChanged={town => setInfo(data => ({...data, town}))}
          initialInputValue={contactInformation.town}
        />
      </ScrollView>
      <Button title={'save'} onPress={saveButtonPress} />
    </SafeAreaView>
  );
};
