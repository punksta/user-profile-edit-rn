// @flow

import * as React from 'react';
import {View, Text} from 'react-native';
import {getConstants} from './constants';

type Props = {
  data: Array<[string, string]>,
  title: string,
};

export const UserProfileConcatInformation = ({data, title}: Props) => (
  <View
    style={{
      paddingHorizontal: getConstants().ScreenHorizontalPadding,
      paddingVertical: 30,
      borderBottomWidth: 1,
      borderColor: 'rgba(255,255,255,0.47)',
    }}>
    <Text
      style={{
        fontSize: 17,
        marginBottom: 18,
      }}>
      {title}
    </Text>

    {data.map(item => {
      const [key, value] = item;
      return (
        <Text
          key={key}
          style={{
            fontSize: 21,
          }}>
          {value}
        </Text>
      );
    })}
  </View>
);
