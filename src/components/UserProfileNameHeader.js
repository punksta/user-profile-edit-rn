// @flow
import React from 'react';
import {View, Text} from 'react-native';
import {getConstants} from './constants';

type Props = {
  name: string,
  userId: string | number,
};
export const UserProfileNameHeader = React.memo<Props>(({name, userId}) => (
  <View
    style={{
      paddingHorizontal: getConstants().ScreenHorizontalPadding,
      paddingVertical: 30,
      borderBottomWidth: 1,
      borderColor: 'rgba(255,255,255,0.47)',
    }}>
    <Text
      style={{
        fontSize: 35,
      }}>
      {name}
    </Text>

    <Text
      style={{
        fontSize: 21,
      }}>
      {userId}
    </Text>
  </View>
));
