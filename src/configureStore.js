import {combineReducers, createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web

import reducers from './reducers';
import {userProfile} from './reducers/userProfile';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['userProfile'],
};

export default () => {
  const persistedReducer = persistReducer(
    persistConfig,
    combineReducers(reducers),
  );
  const store = createStore(persistedReducer);
  return {store, persistor: persistStore(store)};
};
