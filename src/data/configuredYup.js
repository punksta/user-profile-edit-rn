import * as yup from 'yup';

import libphonenumber from 'google-libphonenumber';
const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();

yup.addMethod(yup.string, 'phone', function() {
  return this.test({
    name: 'phone',
    exclusive: true,
    message: 'not a phone number',
    test: value => {
      if (typeof value === 'undefined') {
        return true;
      } else {
        try {
          const phone = phoneUtil.parse(value, 'NL');
          return phoneUtil.isValidNumber(phone);
        } catch (e) {
          return false;
        }
      }
    },
  });
});

export default yup;
