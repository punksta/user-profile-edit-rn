// @flow

import yup from './configuredYup';

export type ContactInformation = {
  postalCode?: number,
  streetName?: string,
  houseNumber?: string,
  flatNumber?: number,
  email: ?string,
  phoneNumber?: string,
  town?: string,
};
export type User = {
  id: string,
  name: string,
  contactInformation: ContactInformation,
};

export const contactInformationScheme = yup.object().shape({
  postalCode: yup
    .number()
    .integer()
    .typeError('not a postal code'),
  streetName: yup.string().typeError('not a street name'),
  houseNumber: yup
    .number()
    .integer()
    .typeError('not a valid house number'),
  flatNumber: yup
    .number()
    .integer()
    .typeError('not a valid flat number'),
  email: yup
    .string()
    .email()
    .typeError('not a valid email'),
  phoneNumber: yup
    .string()
    .notRequired()
    .phone(),
});

export const userScheme = yup.object().shape({
  id: yup.string().required(),
  name: yup.string().required(),
  contactInformation: contactInformationScheme,
});
