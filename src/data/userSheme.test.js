import {userScheme} from './userScheme';

const requiredFieldsUser = {
  name: 'fooBar',
  id: 123,
};

describe('userScheme', () => {
  it('scheme is created', () => {
    expect(userScheme.isValid).toBeInstanceOf(Function);
    expect(userScheme.validate).toBeInstanceOf(Function);
  });

  it('should validates required fields', async () => {
    expect(await userScheme.isValid({})).toBe(false);
    expect(await userScheme.isValid(requiredFieldsUser)).toBe(true);
  });

  it('should validate phone number', async () => {
    expect(
      await userScheme.isValid({
        ...requiredFieldsUser,
        contactInformation: {phoneNumber: 'foob'},
      }),
    ).toBe(false);
    expect(
      await userScheme.isValid({
        ...requiredFieldsUser,
        contactInformation: {phoneNumber: '11'},
      }),
    ).toBe(false);
    expect(
      await userScheme.isValid({
        ...requiredFieldsUser,
        contactInformation: {
          phoneNumber: '0880000000',
        },
      }),
    ).toBe(true);
  });
});
