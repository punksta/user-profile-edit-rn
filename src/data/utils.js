// @flow
export type ValidationError = {
  path?: string,
  message?: string,
  inner?: Array<ValidationError>,
};

export const selectErrorByPath = <U>(
  validationError: ValidationError,
  path: string,
  foundMapper: ValidationError => U,
): ?U => {
  if (path === validationError.path) {
    return foundMapper(validationError);
  } else {
    return (validationError.inner || []).reduce(
      // $FlowFixMe
      (result: ?U, e: ValidationError): ?U =>
        result || selectErrorByPath(e, path, foundMapper),
      undefined,
    );
  }
};

export const selectErrorMessageByPath = (
  validationError: ValidationError,
  path: string,
): ?string => selectErrorByPath(validationError, path, a => a?.message);
