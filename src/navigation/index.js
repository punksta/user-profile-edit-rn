// @flow
import {createStackNavigator} from 'react-navigation';
import * as React from 'react';

import {View, Text} from 'react-native';
import {UserProfileScreen} from '../components/UserProfileScreen';
import {EditProfileScreen} from '../components/EditProfileScreen';

export const createAppNavigator = () =>
  createStackNavigator({
    ShowProfile: {
      screen: UserProfileScreen,
      navigationOptions: {
        title: 'My Profile',
      },
    },
    EditProfile: {
      screen: EditProfileScreen,
      navigationOptions: {
        title: 'Edit Profile',
      },
    },
  });
