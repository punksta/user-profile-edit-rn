import {editProfile} from './editProfile';
import {userProfile} from './userProfile';

export default {
  editProfile,
  userProfile,
};
