// @flow

import {type Action} from 'redux';
import type {User} from '../data/userScheme';
import {ActionTypes} from '../actions';

type State = {
  user: ?User,
};

/**
 *  used when async storage value is empty
 */
const defaultUser: User = {
  name: 'John Smith',
  id: '0001',
  contactInformation: {
    phoneNumber: '0880000000',
    town: 'Amsterdam',
    streetName: 'happy vegans str',
    postalCode: 333,
    flatNumber: 123,
    email: 'John@smith.noemail.com',
  },
};
const defaultState: State = {
  user: defaultUser,
};

export const userProfile = (
  state: State = defaultState,
  action: Action,
): State => {
  switch (action.type) {
    case ActionTypes.CONTACT_INFORMATION_CHANGED:
      return {
        ...state,
        user: {
          ...state.user,
          contactInformation: action.contactInformation,
        },
      };
    default:
      return state;
  }
};

export const selectCurrentUser: (state: {
  userProfile: State,
}) => ?User = state => state.userProfile.user;
