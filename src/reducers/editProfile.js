// @flow

import {type Action} from 'redux';

type State = {
  latestUserForm?: Object,
  isValidating: boolean,
  validationResult: ?Array<Error>,
};

const defaultState = {
  latestUserForm: null,
  isValidating: false,
  validationResult: null,
};

export const editProfile = (state: State = defaultState, action: Action) => {
  switch (action.type) {
    default:
      return state;
  }
};
