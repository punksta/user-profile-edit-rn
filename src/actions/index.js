// @flow

export const ActionTypes = Object.freeze({
  CONTACT_INFORMATION_CHANGED: 'CONTACT_INFORMATION_CHANGED',
});
