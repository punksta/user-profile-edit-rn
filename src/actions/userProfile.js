// @flow

import type {ContactInformation} from '../data/userScheme';
import {ActionTypes} from './index';

export const changeUserProfileContactInformation = (
  contactInformation: ContactInformation,
) => ({
  type: ActionTypes.CONTACT_INFORMATION_CHANGED,
  contactInformation,
});
